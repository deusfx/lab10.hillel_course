https://dropmefiles.com/TPsJN

Поднимаются 3 контейнера описаные с помощью Dockerfile
1. gitserver (копируются ключи, создается репозиторий)
2. webserver (автоматически при билде клонирует репозиторий с помощью ключей и поднимает веб сервер)
3. ubuntu (поднимается, пингует все хосты и выводит curl)

Все в одной сети:
```
roothosts@ubnt-02Andrey:~/lab10.hillel_course$ docker network inspect lab10
[
    {
        "Name": "lab10",
        "Id": "e42092d68652c54d1c46b5611b5902680f6c411bc3aa461e5aaccf38a9d578f5",
        "Created": "2021-02-03T15:03:03.784569805+02:00",
        "Scope": "local",
        "Driver": "bridge",
        "EnableIPv6": false,
        "IPAM": {
            "Driver": "default",
            "Options": {},
            "Config": [
                {
                    "Subnet": "172.20.0.0/16",
                    "Gateway": "172.20.0.1"
                }
            ]
        },
        "Internal": false,
        "Attachable": false,
        "Ingress": false,
        "ConfigFrom": {
            "Network": ""
        },
        "ConfigOnly": false,
        "Containers": {
            "a2f40dfbf22dab0418aa997710ad840b04fb07cb40e69ad9113da7392bab936f": {
                "Name": "webserver",
                "EndpointID": "6513fee0b726fe6fee67d96f1b8c2dc5f867cc6a1d205dc8dc59cfcc8f5e128a",
                "MacAddress": "02:42:ac:14:00:03",
                "IPv4Address": "172.20.0.3/16",
                "IPv6Address": ""
            },
            "b5b198ded032d339e00da8e669d76b0b5c3b2b459c54ab24d00496426fc8ebbd": {
                "Name": "gitserver",
                "EndpointID": "cdfe211a66fdb11f3c98c16ecdb09d2ad17e560aeb3a4c7ab0847fc0ba9634a0",
                "MacAddress": "02:42:ac:14:00:02",
                "IPv4Address": "172.20.0.2/16",
                "IPv6Address": ""
            }
        },
        "Options": {},
        "Labels": {}
    }
]
```


```
roothosts@ubnt-02Andrey:~/lab10.hillel_course$ docker run --net=lab10 ubuntuhealthchecker
PING webserver (172.20.0.3) 56(84) bytes of data.
64 bytes from webserver.lab10 (172.20.0.3): icmp_seq=1 ttl=64 time=0.171 ms
64 bytes from webserver.lab10 (172.20.0.3): icmp_seq=2 ttl=64 time=0.118 ms
64 bytes from webserver.lab10 (172.20.0.3): icmp_seq=3 ttl=64 time=0.107 ms

--- webserver ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 2080ms
rtt min/avg/max/mdev = 0.107/0.132/0.171/0.027 ms
PING gitserver (172.20.0.2) 56(84) bytes of data.
64 bytes from gitserver.lab10 (172.20.0.2): icmp_seq=1 ttl=64 time=0.213 ms
64 bytes from gitserver.lab10 (172.20.0.2): icmp_seq=2 ttl=64 time=0.117 ms
64 bytes from gitserver.lab10 (172.20.0.2): icmp_seq=3 ttl=64 time=0.132 ms

--- gitserver ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 2076ms
rtt min/avg/max/mdev = 0.117/0.154/0.213/0.042 ms
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100   251  100   251    0     0  22818      0 --:--:-- --:--:-- --:--:-- 22818
<!doctype html>
<html>
  <head>
    <title>Build Dockerfile lab</title>
  </head>
  <body>
    <p>This is an example page. Docker build dockerfile and run container with nginx.</p>
    <img src="images/docker-logo.png" alt="docker">
  </body>
</html>
roothosts@ubnt-02Andrey:~/lab10.hillel_course$
```

